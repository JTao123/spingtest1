<!DOCTYPE html>
<html lang="en">
<head>
    <style type="text/css">

        .nav{
            width:800px;
            background: #E8E8D0;
            margin: 20px;
        }
        .1{
            width: 150px;

        }
        .2{
            width: 300px;
        }
        .3{
            width: 150px;
        }
        .4{
            width: 200px;
        }

        a:link {text-decoration: none;color: blue}		/* 未访问的链接 */
        a:visited {text-decoration: none;color: blue }	/* 已访问的链接 */
        a:hover {text-decoration:underline;color: #FF00FF}	/* 鼠标移动到链接上 */
        a:active {text-decoration: none;color: #0000FF}	/* 选定的链接 */

    </style>
    <meta charset="UTF-8">
    <title>图书管理</title>

</head>
<body>
<center>
    <br>
<h1>图书管理系统</h1>
    <br>
    <br>
    <form method="post" action="/book/list2">
        图书名称:<input type="text" name="name" />&nbsp;
        图书作者:<input type="text" name="author" />
        <input type="submit" value="检索">
    </form>
    <br>
    <table class="nav" align="center">
    <tr>
        <th class="1">编号</th>
        <th class="2">图书名称</th>
        <th class="3">图书作者</th>
        <th class="4">操作</th>
    </tr>
    <#list bookList as book>
    <tr>
        <td class="1" align="center">${book.id}</td>
        <td class="2" align="center">${book.name}</td>
        <td class="3" align="center">${book.author}</td>
        <td class="4" align="center">
            <a href="/book/preUpdate/${book.id}">修改</a>
                <a href="/book/delete?id=${book.id}">删除</a>
        </td>
    </tr>
    </#list>
</table>
    <br>
    <a href="/add.html">添加新的图书</a>
    <br>
</center>
</body>
</html>