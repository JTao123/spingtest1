package com.controller;

import com.dao.BookDao;
import com.entity.Book;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Controller
@RequestMapping("/book")
public class BookController {

    @Resource
    private BookDao bookDao;

    @RequestMapping("/list")
    public ModelAndView list(){
        ModelAndView mav= new ModelAndView();
        mav.addObject("bookList",bookDao.findAll());
        mav.setViewName("bookList");
        return mav;
    }

    @RequestMapping(value ="/add",method = RequestMethod.POST)
    public String add(Book book){
        bookDao.save(book);
        return "redirect:/book/list";
    }

    @RequestMapping("/preUpdate/{id}")
    public ModelAndView preUpdate(@PathVariable("id") Integer id){
        ModelAndView mav=new ModelAndView();
        mav.addObject("book",bookDao.getOne(id));
        mav.setViewName("update");
        return mav;
    }

    @RequestMapping(value ="/update",method = RequestMethod.POST)
    public String update(Book book){
        bookDao.save(book);
        ModelAndView modelAndView=new ModelAndView();
        modelAndView.addObject("bookList",bookDao.findAll());
        return "redirect:/book/list";
    }

    @GetMapping("/delete")
    public String delete(Integer id){
        bookDao.deleteById(id);
        return "redirect:/book/list";
    }

    @ResponseBody
    @GetMapping("/queryByName")
    public List<Book> queryByName(){
        return bookDao.findByName("技术");
    }

    @ResponseBody
    @GetMapping("/randomList")
    public List<Book> randomList(){
        return bookDao.randomList(2);
    }

    @RequestMapping("list2")
    public ModelAndView list2(Book book) {
        ModelAndView mav = new ModelAndView();
        List<Book> bookList =bookDao.findAll(new Specification<Book>() {
            @Override
            public Predicate toPredicate(Root<Book> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Predicate predicate=criteriaBuilder.conjunction();
                if (book!=null){
                    if(book.getName()!=null && !"".equals(book.getName())){
                        predicate.getExpressions().add(criteriaBuilder.like(root.get("name"),"%"+book.getName()+"%"));
                    }
                    if (book.getAuthor()!=null && !"".equals(book.getAuthor())){
                        predicate.getExpressions().add(criteriaBuilder.like(root.get("author"),"%"+book.getAuthor()+"%"));
                    }
                }
                return predicate;
            }
        });
        mav.addObject("bookList",bookList);
        mav.setViewName("bookList");
        return mav;
    }

}