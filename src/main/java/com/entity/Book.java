package com.entity;

import javax.persistence.*;

@Entity
@Table(name="t_book")
public class Book {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(length = 100)
    private String name;

    @Column(length = 50)
    private String author;

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getId() {

        return id;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }
}
